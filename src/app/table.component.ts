import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormControl } from "@angular/forms";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { DataService } from "./data.service";
import { DataRecord, RecordFilter } from "./models/record.model";

@Component({
  selector: "mytable",
  templateUrl: "./table.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      h1 {
        font-family: Lato;
      }
    `
  ]
})
export class TableComponent implements OnInit, OnDestroy{
  products$: Observable<DataRecord[]>;

  constructor(
    private http: HttpClient,
    private _dataService: DataService,
    private _formBuilder: FormBuilder,
  ) {
    this.products$ = this.getProducts();
  }

  readonly recordColumn: (keyof DataRecord)[] = [
    "code",
    "name",
    "category",
    "quantity",
    "price"
  ];

  readonly headerForm = this._formBuilder.group(<Record<keyof RecordFilter, FormControl>>{
    id: this._formBuilder.control(""),
    category: this._formBuilder.control(""),
    code: this._formBuilder.control(""),
    description: this._formBuilder.control(""),
    image: this._formBuilder.control(""),
    inventoryStatus: this._formBuilder.control(""),
    name: this._formBuilder.control(""),
    price: this._formBuilder.control(""),
    quantity: this._formBuilder.control(""),
    rating: this._formBuilder.control(""),
  });

  sortedHeader?: keyof DataRecord;
  /** map from column to its colapse boolean state */
  collapseState: Partial<Record<keyof DataRecord, boolean>>  = {
  }


  sortAsc: boolean = false;


  private _destroy$ = new Subject();

  ngOnInit(): void {
    this.headerForm.valueChanges.pipe(
      takeUntil(this._destroy$),
    ).subscribe((filter: RecordFilter) => this._dataService.setFilter(filter))
  }

  ngOnDestroy(): void {
    // emit value to complete listeners
    this._destroy$.next();
    // complete observable to finalize any accidantal subscriptions
    this._destroy$.complete();
  }

  getProducts() {
    return this._dataService.getData();
  }

  sortBy(header: keyof DataRecord){
    if(this.sortedHeader === header){
      this.sortAsc = !this.sortAsc;
    }
    this.sortedHeader = header;
    this._dataService.setSort(
      this.sortedHeader,
      this.sortAsc
    );
  }

  toggleCollapseState($event: MouseEvent, header: keyof DataRecord){
    this.collapseState[header] = !this.collapseState[header];
    $event.stopPropagation();
  }
}
