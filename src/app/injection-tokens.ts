import { InjectionToken } from "@angular/core";



export const POLLING_INTERVAL = new InjectionToken<number>('config-poll-interval-ms', {
  providedIn: 'root',
  factory: () => 1000_000, // 1 second interval
})