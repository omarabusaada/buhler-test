import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { BehaviorSubject, interval, of } from "rxjs";
import {
  catchError,
  debounceTime,
  map,
  startWith,
  switchMap,
} from "rxjs/operators";
import { POLLING_INTERVAL } from "./injection-tokens";
import { DataRecord, RecordFilter } from "./models/record.model";
import { ServerResponse } from "./models/server-response.model";
import { SortType } from "./models/sort.model";

/** (for lack of appropriate name) DataService is responsible for fetching and processing data */
@Injectable()
export class DataService {
  constructor(
    private _http: HttpClient,
    @Inject(POLLING_INTERVAL) private readonly _pollInterval: number,
    ) {}

  /** used to apply next filter to the fetched data */
  private readonly __filter$ = new BehaviorSubject<Partial<DataRecord>>({});
  /** emits the filter object */
  private readonly _fitler$ = this.__filter$.pipe(
    // do any processing on the filter here
    startWith(this.__filter$.value),
    debounceTime(100)
  );

  /** whether to sort in ascending order or descending,
   * currently only two types are supported:
   * * asc - ascending order
   * * desc - descending order
   */
  private __sort$ = new BehaviorSubject<{ field: keyof DataRecord; order: SortType}>({field: undefined, order: "none"});
  private _sort$ = this.__sort$.pipe(debounceTime(100));

  setFilter(filter: RecordFilter) {
    this.__filter$.next(filter);
  }

  setSort(field: keyof DataRecord, asc: boolean) {
    this.__sort$.next({ field, order: asc ? "asc" : "desc" });
  }

  getData() {
    // we use interval to auto-refresh the page on a timer
    // provided we have a backend, we can implement proper log-polling or
    // even web-socket
    return interval(this._pollInterval).pipe(
      startWith(0),
      switchMap(() =>
        this._http.get<ServerResponse<DataRecord>>("assets/mock.data.json")
      ),
      map((results) => results.data),
      switchMap((response) =>
        this._fitler$.pipe(
          map(this._applyFilters(response)),
          catchError((err) => {
            // handle error here
            return of([] as DataRecord[]);
          })
        )
      ),
      switchMap((results) =>
        this._sort$.pipe(
          // startWith({ field: undefined, order: <SortType>"none" }),
          map(({ field, order }) => {
            if (!field) return results;
            return this.sortItemsByField(results, order, field);
          }),
          catchError((err) => {
            // handle error here
            console.error(err);
            return of(results);
          })
        )
      )
    );
  }

  /** filter a data-array by a given filter
   * the filter is a partial object of the same structure as the data-item
   * filter by: convert property to string and use startWith
   * should be pure function, but just in case preserve `this`
   */
  private readonly _applyFilters =
    (data: DataRecord[]) =>
    (filter: Partial<DataRecord>): DataRecord[] =>
      data.filter((item) => this.itemMatchesFilter(item, filter));

  /**
   * return true if item matches filter
   * return false otherwise
   */
  private readonly itemMatchesFilter = (
    item: DataRecord,
    filter: Partial<DataRecord>
  ) => {
    const filterKeys = Object.keys(filter) as (keyof typeof filter)[];
    if (filterKeys.length === 0) return true;
    return filterKeys.every((filterKey) => {
      return item[filterKey]
        .toString()
        .toLowerCase()
        .startsWith(filter[filterKey].toString().toLowerCase());
    });
  };

  private readonly sortItemsByField = (
    items: DataRecord[],
    sort: SortType,
    field: keyof DataRecord
  ) =>
    sort === "none"
      ? items
      : items.sort((a, b) => this.compareItems(a[field], b[field], sort));

  private readonly compareItems = (
    a: DataRecord[keyof DataRecord],
    b: DataRecord[keyof DataRecord],
    sortOp: SortType
  ): number => {
    const compFn = this.sortComparators[sortOp];
    if (!sortOp) {
      // report error
      try {
        throw new Error("unknown sort operationi " + sortOp.toString());
      } catch (err) {
        console.error(err);
        return 0;
      }
    }

    return compFn(a, b);
  };

  private sortComparators: Record<
    SortType,
    <T extends DataRecord[keyof DataRecord]>(a: T, b: T) => number
  > = {
    asc: (a, b) => {
      switch (true) {
        case typeof a === "string":
          return (<string>b).localeCompare(<string>a);
        case typeof a === "number":
          return <number>a - <number>b;
      }
    },
    desc: (a, b) => -1 * this.sortComparators.asc(a, b),
    // when we're not sorting by any field, all strings are equivalent
    none: (a, b) => 0,
  };
}
