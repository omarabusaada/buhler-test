import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, OnInit } from "@angular/core";


@Directive({
  selector: "[badge]",
  host: {
    // append a special class to the host element in order to be able to customize its style
    'class': 'badge-host',
  }
})
export class BadgeDirective implements OnInit, OnDestroy, AfterViewInit{
  @Input() badge: string;
  constructor(
    private _host: ElementRef<HTMLElement>,
  ){}


  private badgeElem?: HTMLElement;

  ngOnInit(): void {
  }
  
  ngOnDestroy(): void {
    this.badgeElem?.remove();
  }
  
  ngAfterViewInit(): void {
    if(this.badge) this.badgeElem = this._createTemplateWithBadge();
    this._host.nativeElement.appendChild(this.badgeElem);
  }


  private _createTemplateWithBadge(){
    const badge = document.createElement("span");
    badge.classList.add("badge");
    badge.textContent = this.badge;
    return badge;
  }
}