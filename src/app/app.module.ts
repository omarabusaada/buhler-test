import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { TableComponent } from "./table.component";
import { HttpClientModule } from "@angular/common/http";
import { DataService } from "./data.service";
import { BadgeDirective } from "./badge.directive";

@NgModule({
  imports: [BrowserModule, FormsModule, HttpClientModule, ReactiveFormsModule, ],
  declarations: [AppComponent, TableComponent, BadgeDirective],
  bootstrap: [AppComponent],
  providers: [DataService]
})
export class AppModule {}
