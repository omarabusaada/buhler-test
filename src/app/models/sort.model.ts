

export type SortType = 
  "asc" |
  "desc" | 
  "none";