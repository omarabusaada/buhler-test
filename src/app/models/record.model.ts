

/*

sample record:
--------------

			"id": "1000",
			"code": "f230fh0g3",
			"name": "Bamboo Watch",
			"description": "Product Description",
			"image": "bamboo-watch.jpg",
			"price": 65,
			"category": "Accessories",
			"quantity": 24,
			"inventoryStatus": "INSTOCK",
			"rating": 5

*/

/** represents the structure of a single record of the data */
export interface DataRecord{
  id: number;
  code: string;
  name: string;
  description: string;
  /** name if an image file, without path */
  image: string;
  price: number;
  category: string;
  quantity: string;
  inventoryStatus: InventoryStatusType;
  rating: number;
}

/** represents a filter to be applied on a list of data-records */
export type RecordFilter = Partial<DataRecord>;


/** @todo convert into proper enum type */
export type InventoryStatusType = string;