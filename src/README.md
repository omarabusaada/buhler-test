# Buhler Front-end Test

Thanks for considering joining us here's a small test to see your skills.
Please don't use any component libaries directly however feel free to take inspiration. (icon libs allowed)

### Features to Implement

Table structure is important for displaying data; 
Here's a list of requestments but feel free to personalise it with CSS or anything you'd like to add. However the fllowing will be looked at.

Feature:
* Global Search - User should be able to filter/search the table data relative to ANY match content. Properties (sorted) by StartWith.

* Ascending/Descending Sort functionality on clicking of header cells

* Expanding/collapse functionality showing properties of Product item (see mock.data.json) { code, description }.

* Write a directive: That appends to a button/badge with showing inventory status on Product (see mock.data.json) {inventoryStatus}

* Bonus: Create a way of handling the data as if it's "live" randomly changing inventory status.

* Extra Merit: 
   * Good styling/design + structure